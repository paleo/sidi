# Sidi

Sidi (Simple Distributed) is a tiny Python module on top of [dask.distributed](http://distributed.dask.org).

It brings three main features:

  * The `DaskScheduler` object
  * The `DaskWorker` object
  * The `RemoteClass` object

The first two aim at making easier the instantiation of distributed scheduler and workers from Python scripts.
The latest is a wrapper around `distributed.Actor`.