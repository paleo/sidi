{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Using `dask.distributed` for distributed computing\n",
    "\n",
    "In this notebook, we illustrate how we can use `dask.distributed` in a front-end machine to distribute computation on remote nodes, here tomography Filtered Back Projection of a series of sinograms.\n",
    "\n",
    "## 1. distributed in a nutshell\n",
    "\n",
    "### 1.1 - Basic concepts\n",
    "\n",
    "\n",
    "`distributed` provides three fundamental entities:\n",
    "  - The *Workers*: these are abstracted views on process/thread running either on the local machine, or on a remote cluster. As their name suggest, workers handle the tasks submitted to them.\n",
    "  - The *Scheduler*: its role is to manage the tasks distribution to the workers. It notably inspects workers state, tasks progress, and can handle workers failure (redispatching work).\n",
    "  - The *Client* which allows the user to connect to the scheduler and/or workers, and to submit work.\n",
    " \n",
    "\n",
    "The client submits work to the scheduler, which, in turn, dispatches tasks to the workers.\n",
    "The workers can be instantiated either on the local machine, or through a batch scheduler (SLURM, OAR, SGE, ...) (see for example `dask_jobqueue.SLURMCluster`).  \n",
    "`distributed` also provides a `Cluster` object with instantiates both the scheduler and workers.  \n",
    "A notable feature of the scheduler is the intelligent handling of workers: work distribution, resilience, monitoring, computational graph representation, and so on. Thus, a notable amount of \"dirty work\" is taken care of by the scheduler, leaving the programmer a convenient interface.\n",
    "\n",
    "\n",
    "### 1.2 - Setup a cluster\n",
    "\n",
    "A distributed cluster regroups the two entities `(scheduler, workers)`. \n",
    "There are several ways to spawn a cluster:\n",
    "  - From the command line, with the CLI utilities `dask-scheduler` and `dask-worker`.\n",
    "  - From a Python script, for example using `LocalCluster` (for multi-processing on the same machine).\n",
    "  \n",
    "Once the cluster is created, we must also create a `Client` instance to connect to the cluster's scheduler and submit tasks.\n",
    "\n",
    "The scheduler has to be spawned first. Then, workers contact the scheduler to \"register\" to it, so that the scheduler is aware of the total number of available workers.\n",
    "\n",
    "In this notebook, we focus on the second way to spawn a cluster.\n",
    "\n",
    "### 1.3 - Some advanced features\n",
    "\n",
    "This section can be skipped in the first read.\n",
    "\n",
    "`distributed` offers many appealing features, among which:\n",
    "  - Network interfaces: the communication between scheduler and workers can be done via any existing network interface, notably Infiniband (ex. `dask-scheduler --interface ib0`)\n",
    "  - Lazy data loading: the client can submit a \"data load\" operation in the form of a computational graph node. This enables to load chunks of data only by (remote) workers, instead of transferring it. The data can be a `dask.delayed` or `pandas.DataFrame` container. HDF5 is only partially supported (through pytables).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# First makes sure all packages required to run this notebook are installed\n",
    "# Restart the notebook if packages were installed\n",
    "# %pip install git+https://gitlab.esrf.fr/paleo/sidi.git dask-jobqueue numpy silx pyopencl Mako\n",
    "%pip install dask-jobqueue numpy silx pyopencl Mako matplotlib ipympl ipywidgets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import os, sys\n",
    "\n",
    "from distributed import Client\n",
    "from dask_jobqueue import OARCluster\n",
    "from silx.opencl.backprojection import Backprojection\n",
    "from sidi.remote import RemoteClass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Download the sinograms used for the reconstruction later\n",
    "SINOGRAMS_FILE = os.path.join(os.getcwd(), \"MRI512_proj500.npz\")\n",
    "\n",
    "if not os.path.exists(SINOGRAMS_FILE):\n",
    "    !wget http://www.silx.org/pub/sidi/MRI512_proj500.npz -O MRI512_proj500.npz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Display the sinograms\n",
    "%matplotlib inline\n",
    "\n",
    "from ipywidgets import interactive, widgets\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "def show_sino(sino_num):\n",
    "    plt.figure()\n",
    "    plt.imshow(np.load(SINOGRAMS_FILE)['data'][sino_num])\n",
    "    plt.colorbar()\n",
    "    plt.show()\n",
    "\n",
    "interactive(\n",
    "    show_sino,\n",
    "    sino_num=widgets.IntSlider(value=0, min=0, max=512, continuous_update=False))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Creating a cluster\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 - Spawning workers nodes through OAR\n",
    "\n",
    "We first start by the resource description. The `local_directory` is used to store `stdout` and `stderr`, along with  `distributed` logs and internal state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "TMP_DIR = os.path.join(os.getcwd(), \"tmp\", \"dask\")\n",
    "\n",
    "STORAGE_DIR = os.path.join(TMP_DIR, \"storage\")\n",
    "os.makedirs(STORAGE_DIR, exist_ok=True)\n",
    "\n",
    "resource_description = {\n",
    "    \"queue\": \"gpu\",\n",
    "    \"cores\": 2,\n",
    "    \"processes\": 1,\n",
    "    \"memory\": \"10GB\",\n",
    "    \"project\": 'dasktest',\n",
    "    \"walltime\": '00:15:00',\n",
    "    \"local_directory\": STORAGE_DIR,\n",
    "    \"silence_logs\": 0,\n",
    "    \"python\": sys.executable\n",
    "}\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "num_workers = 2\n",
    "# Create the OARCluster. It only loads the description.\n",
    "oar_cluster = OARCluster(**resource_description)\n",
    "print(\"Cluster description created\")\n",
    "oar_cluster.scale(num_workers) # Actually asks for nodes. This takes some time.\n",
    "print(\"Spawned %d workers on OARCluster\" % num_workers)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's it, the cluster is created with two workers! If another batch scheduler is used, it merely boils down to changing the call with `SLURMCluster`, `SGECluster`, etc.  \n",
    "You can run `oarstat | grep dasktest` on a front-end machine to check that the resources were allocated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import subprocess\n",
    "\n",
    "result = subprocess.run([\"oarstat\"], capture_output=True, text=True)\n",
    "[line for line in result.stdout.split('\\n') if resource_description[\"project\"] in line]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that `distributed` has a nice Jupyter integration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "oar_cluster"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 - Connecting to the cluster with a client\n",
    "\n",
    "Now we create the client which connects to the scheduler. As the `oar_cluster` object bears all necessary information, things are simple:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "client = Client(oar_cluster)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the case where the cluster is not spawned by the same process (i.e., we cannot access a `oar_cluster` object), we simply call `Client(scheduler_ip_address)`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3 Instantiating a class on a remote node"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.1 - Class definition\n",
    "\n",
    "In the following cell, we define a `RemoteFBP` class which wraps `silx.opencl.backprojection` with logging and some additional controls."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import os, sys\n",
    "from glob import glob\n",
    "import numpy as np\n",
    "\n",
    "class RemoteFBP(object):\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        kwargs = self.setup_logger(**kwargs)\n",
    "        self._c = Backprojection(*args, **kwargs)\n",
    "        for what in dir(self._c):\n",
    "            if what != \"_c\" and not(what.startswith(\"__\")):\n",
    "                setattr(self, what, getattr(self._c, what))\n",
    "        self.pid = os.getpid()\n",
    "        self.filename = None\n",
    "        self.path = None\n",
    "        print(\"[%d] Remote initiated on device %s\" % (self.pid, self._c.device), flush=True)\n",
    "\n",
    "    def setup_logger(self, **kwargs):\n",
    "        self.logger_file = None\n",
    "        if \"logger_file\" in kwargs:\n",
    "            self.logger_file = kwargs.pop(\"logger_file\")\n",
    "            self._log_fid = open(self.logger_file, \"w\")\n",
    "            self.old_stdoud = sys.stdout\n",
    "            sys.stdout = self._log_fid\n",
    "        return kwargs\n",
    "        \n",
    "    def get_sino(self, slice_num):\n",
    "        if self.filename is None:\n",
    "            raise RuntimeError(\"Input filename not set yet\")\n",
    "        return np.load(self.filename)['data'][slice_num]\n",
    "\n",
    "    def reconstruct_slice(self, slice_num):\n",
    "        if self.path is None:\n",
    "            raise RuntimeError(\"Output path not set yet\")\n",
    "        pid = os.getpid()\n",
    "        print(\"[%d] Loading slice %d\" % (pid, slice_num), flush=True)\n",
    "        data = self.get_sino(slice_num)\n",
    "        print(\"[%d] Reconstructing\" % pid, flush=True)\n",
    "        res = self.filtered_backprojection(data)\n",
    "        print(\"[%d] OK\" % pid, flush=True)\n",
    "        out_dir = os.path.join(self.path, \"output\")\n",
    "        if not(os.path.isdir(out_dir)):\n",
    "            os.mkdir(out_dir)\n",
    "        fname = os.path.join(out_dir, \"fbp_%04d.npy\" % slice_num)\n",
    "        np.save(fname, res)\n",
    "        \n",
    "    def reconstruct_slices_range(self, slices_range):\n",
    "        for s in slices_range:\n",
    "            self.reconstruct_slice(s)\n",
    "\n",
    "    def set_sino_filename(self, filename):\n",
    "        self.filename = filename\n",
    "        self.path = os.path.abspath(os.path.dirname(filename))\n",
    "    \n",
    "    def get_device(self):\n",
    "        return self._c.device\n",
    "    \n",
    "    def __del__(self):\n",
    "        if self.logger_file is not None:\n",
    "            self._log_fid.close()\n",
    "            sys.stdout = self.old_stdout\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2 - Instantiating the class on a remote node\n",
    "\n",
    "We use the `sidi.RemoteClass` wrapper around `distributed.Actor`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "LOG_DIR = os.path.join(TMP_DIR, \"logs\")\n",
    "os.makedirs(LOG_DIR, exist_ok=True)\n",
    "\n",
    "print(\"Instantiating workers...\", end=\"\")\n",
    "# This creates one RemoteFBP class instance per worker node.\n",
    "R = RemoteClass(\n",
    "    client,\n",
    "    RemoteFBP,\n",
    "    class_args=[\n",
    "        (500, 512),\n",
    "    ],\n",
    "    class_kwargs_list=[\n",
    "        {\"logger_file\": os.path.join(LOG_DIR, \"worker1.log\")},\n",
    "        {\"logger_file\": os.path.join(LOG_DIR, \"worker2.log\")}\n",
    "    ]\n",
    ")\n",
    "print(\"... OK\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.3 - Submitting tasks\n",
    "\n",
    "When submitting a task, the \"remote class\" returns a (list of) `distributed.future` object(s) (awaitable equivalent to `concurrent.futures`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "R.actors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now submit tasks to the set of workers underlying the `RemoteClass` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "futs = R.submit_task(\n",
    "    \"set_sino_filename\", \n",
    "    method_args=(SINOGRAMS_FILE, ),\n",
    "    who=\"all\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "futs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "futs = R.submit_task(\"get_device\", who=\"all\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "list(map(lambda f : f.result(), futs))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we submit heavy operation to the workers. When not specifying `who=\"all\"`, the task is submitted to the first available worker. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "futures = []\n",
    "futures.extend(R.submit_task(\"reconstruct_slices_range\", method_args=(range(140, 240),)))\n",
    "futures.extend(R.submit_task(\"reconstruct_slices_range\", method_args=(range(240, 340),)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import glob\n",
    "\n",
    "print('Available slices:', len(glob.glob(os.path.join(os.getcwd(), 'output', '*'))))\n",
    "\n",
    "futures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, display reconstructed slices"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%ls output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def show_slice(slice_num):\n",
    "    plt.figure()\n",
    "    plt.imshow(np.load(os.path.join('output', 'fbp_%04d.npy' % slice_num)))\n",
    "    plt.colorbar()\n",
    "    plt.show()\n",
    "\n",
    "interactive(\n",
    "    show_slice,\n",
    "    slice_num=widgets.IntSlider(value=240, min=140, max=339, continuous_update=False))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
