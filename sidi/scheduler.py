from multiprocessing import Process
from tornado.ioloop import IOLoop
from distributed import Scheduler
from distributed.cli.utils import install_signal_handlers

from .utils import generate_random_string
from .logger import Logger

class DaskScheduler(object):
    """
    Initialize a dask scheduler.

    Parameters
    ----------
    addr: str, optional
        Scheduler listening address.
    port: int, optional
        Scheduler listening port.
    name: str, optional
        Name of the scheduler.
    scheduler_file: str, optional
        File where the information on scheduler will be stored.
    logging_options: dict, optional
        Dict where the keys are parameters of the Logger() class.
    bokeh_port: int, optional
        If set, the scheduler will propose a Bokeh service at the given port.

    Notes
    -----
    The scheduler has to use its own event loop (tornado.IOLoop()).
    As the GIL prevents to effectively use different event loops in separate threads,
    the scheduler has to be run in a separate process.
    """
    def __init__(
        self,
        addr="tcp://127.0.0.1",
        port=5454,
        name=None,
        scheduler_file=None,
        logging_options=None,
        bokeh_port=None,
    ):
        self.port = port
        self.addr = addr + ":" + str(port)
        self.name = name or "scheduler_" + generate_random_string(10)
        self.scheduler_file = scheduler_file or "/tmp/" + self.name
        self.configure_logger(logging_options)
        self.logger.debug("Initializing Scheduler '%s'" % self.name)
        self.loop = IOLoop.current()
        self.logger.debug("Using loop %s" % str(self.loop))
        self.services = {}
        self.configure_bokeh(bokeh_port)

        self.scheduler = Scheduler(
            loop=self.loop,
            services=self.services,
            scheduler_file=self.scheduler_file
        )
        install_signal_handlers(self.loop, cleanup=self.cleanup)


    def configure_logger(self, logging_options):
        default_options = {
            "loggername": self.name,
            "level": "DEBUG",
            "logfile": self.name + ".log",
            "console": True,
            "capture_stderr": True,
        }
        if logging_options is not None:
            default_options.update(logging_options)
        else:
            logging_options = default_options
        loggername = logging_options.pop("loggername")
        self.logger = Logger(loggername, **logging_options)

    def configure_bokeh(self, bokeh_port, bokeh_prefix=None):
        if bokeh_port is not None:
            from distributed.bokeh.scheduler import BokehScheduler
            self.services[('bokeh', bokeh_port)] = (BokehScheduler, {'prefix': bokeh_prefix})
            self.logger.info("Bokeh at %s:%d" % (bokeh_prefix, bokeh_port))


    def start(self, start_ioloop=True):
        """
        Start the scheduler.
        """
        self.logger.info("Starting scheduler at %s" % self.addr)
        self.scheduler.start(self.addr)
        try:
            if start_ioloop:
                self.logger.debug("Starting scheduler loop")
                self.loop.start()
                self.loop.close()
                self.logger.debug("Stopping scheduler loop")
        finally:
            self.logger.info("Stopping scheduler")
            self.scheduler.stop()


    def cleanup(self, signum):
        self.logger.critical("Received signal %s" % str(signum))
        pass


def run_scheduler(**scheduler_args):
    """
    This function should be run in another process.
    Both class instantiation and start() should be run on a separate process,
    as the event loop is created in the instantiation.
    """
    S = DaskScheduler(**scheduler_args)
    S.start()


def spawn_scheduler_process(**scheduler_args):
    Ps = Process(target=run_scheduler, kwargs=scheduler_args)
    Ps.start()
    return Ps

