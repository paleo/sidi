from multiprocessing import cpu_count
from tornado import gen
from tornado.ioloop import IOLoop
from distributed import Worker, Nanny
from distributed.cli.utils import install_signal_handlers
from distributed.utils import get_ip_interface, parse_timedelta
from .utils import generate_random_string
from .logger import Logger


_ncores = cpu_count()

class DaskWorker(object):
    """
    Dask Worker/Nanny.

    Parameters
    -----------
    scheduler_addr: str
        Address of the scheduler, ex. tcp://127.0.0.1:8786
    nprocs: int, optional
        Number of worker processes to launch.
    nthreads: int, optional
        Number of threads per worker worker
    host: str, optional
        Worker listening host
    port: int, optional
        Worker listening port

    """

    def __init__(
        self,
        scheduler_addr,
        nprocs=1,
        nthreads=None,
        logging_args=None,
        host=None,
        port=None,
        use_nanny=True,
        interface=None,
        name=None,
        death_timeout=None,
        loop=None,
        memory_limit = "auto",
        local_directory = "/tmp",
        services = {},
        resources = None,
    ):

        self.name = name or "worker_" + generate_random_string(10)
        self.configure_logger(logging_args)
        self.logger.debug("Initializing worker '%s'" % self.name)
        self.use_nanny = use_nanny

        if not nthreads:
            # Share the available threads among workers
            nthreads = _ncores // nprocs

        self.loop = loop or IOLoop.current()
        self.logger.debug("Using loop %s" % str(self.loop))
        self.own_loop = True

        if use_nanny:
            kwargs = {'worker_port': port, 'listen_address': None} # TODO
            what = Nanny
        else:
            kwargs = {}
            #~ kwargs['service_ports'] = {'nanny': port} # TODO
            what = Worker

        if interface:
            if host:
                raise ValueError("Can not specify both interface and host")
            else:
                host = get_ip_interface(interface)

        if host is not None and port is not None:
            self.addr = host + port
        else:
            # Choose appropriate address for scheduler
            self.addr = None

        if death_timeout is not None:
            death_timeout = parse_timedelta(death_timeout, 's')

        # TODO support bokeh ?
        self.nannies = [
            what(
                scheduler_addr,
                ncores=nthreads,
                services=services,
                loop=self.loop,
                resources=resources,
                memory_limit=memory_limit,
                reconnect=True,
                local_dir=local_directory,
                death_timeout=death_timeout,
                name=self.name if nprocs == 1 else self.name + '-' + str(i),
                **kwargs
            )
            for i in range(nprocs)
        ]
        install_signal_handlers(self.loop, cleanup=self.on_signal)


    @gen.coroutine
    def close_all(self):
        # Unregister all workers from scheduler
        if self.use_nanny:
            yield [n._close(timeout=2) for n in self.nannies]


    def on_signal(self, signum):
        self.logger.critical("Exiting on signal %d" % signum)
        self.close_all()


    @gen.coroutine
    def run(self):
        yield [n._start(self.addr) for n in self.nannies]
        while all(n.status != 'closed' for n in self.nannies):
            yield gen.sleep(0.2)
        self.logger.debug("All workers closed")


    def start(self):
        try:
            self.loop.run_sync(self.run)
        except Exception as exc:
            # Another was already started with the same IOLoop
            # There must be a way to detect that IOLoop is not owned by current thread ?
            if str(exc) == "IOLoop is already running":
                self.logger.warning("IOLoop %s is already used" % str(self.loop))
                self.own_loop = False
            else:
                self.logger.fatal("Exception: %s" % exc) # logger.exception ?
                # raise ?
        finally:
            if self.own_loop:
                self.logger.info("Stopping worker '%s'" % self.name)


    def stop(self):
        self.logger.debug("Attempting to terminate workers")
        for n in self.nannies:
            if self.use_nanny:
                n.kill()
            else:
                n.stop()
            n.status = "closed"


    # TODO factor with scheduler
    def configure_logger(self, logging_options):
        default_options = {
            "loggername": self.name,
            "level": "DEBUG",
            "logfile": self.name + ".log",
            "console": True,
            "capture_stderr": True,
        }
        if logging_options is not None:
            default_options.update(logging_options)
        else:
            logging_options = default_options
        loggername = logging_options.pop("loggername")
        self.logger = Logger(loggername, **logging_options)



def run_daskworker(**daskworker_args):
    """
    This function should be run in another process.
    Both class instantiation and start() should be run on a separate process,
    as the event loop is created in the instantiation.
    """
    S = DaskWorker(**daskworker_args)
    S.start()


def spawn_worker_process(**worker_args):
    Ps = Process(target=run_daskworker, kwargs=daskworker_args)
    Ps.start()
    return Ps



