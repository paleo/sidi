from random import choice
from string import ascii_letters, digits
#~ from psutil import cpu_count, virtual_memory, net_if_addrs, cpu_freq
import psutil
from socket import gethostname
from .gpuinfo import get_nvidia_gpuinfo



def generate_random_string(length):
    result = ""
    alphabet = ascii_letters + digits
    for i in range(length):
        result += choice(alphabet)
    return result



def get_mem_info():
    return psutil.virtual_memory()

def get_cpu_info():
    return psutil.cpu_count(), psutil.cpu_freq()



def get_machine_info():
    meminfo = get_mem_info()
    cpuinfo = get_cpu_info()
    try:
        nv_gpuinfo = get_nvidia_gpuinfo()
    except:
        nv_gpuinfo = None
    M = 1024.**3
    infos = {
        "hostname": gethostname(),
        "cpu_count": cpuinfo[0],
        "cpu_maxfreq": str("%.2f GHz" % (cpuinfo[1].max/1e3)),
        "mem_total": str("%.1f GB" % (meminfo.total/M)),
        "mem_used": str("%.1f GB" % (meminfo.used/M)),
        "mem_avail": str("%.1f GB" % (meminfo.available/M)),
        "gpu_count": 0,
    }
    if nv_gpuinfo is not None:
        gpu_infos = {
            "gpu_count": nv_gpuinfo["attached_gpus"],
            "gpu": [],
        }
        for ginfos in nv_gpuinfo["gpu"]:
            gpu_infos["gpu"].append({
                "name": ginfos["product_name"],
                "mem_total": ginfos["fb_memory_usage"]["total"],
                "mem_used": ginfos["fb_memory_usage"]["used"]
            })
        infos.update(gpu_infos)
    return infos



"""
def print_machine_info():
    meminfo = get_mem_info()
    cpuinfo = get_cpu_info()
    try:
        nv_gpuinfo = get_nvidia_gpuinfo()
    except:
        nv_gpuinfo = None
    M = 1024.**3
    print("Hostname: \t %s" % gethostname())
    print("CPUs: \t\t %d (%.1f GHz)" % (cpuinfo[0], cpuinfo[1].max/1e3))
    print("Memory: \t %.1f GB (%.1f GB used)" % (meminfo.total/M, meminfo.used/M))
    if nv_gpuinfo is not None:
        print("GPUs: %s" % nv_gpuinfo["attached_gpus"])
        for i, ginfos in enumerate(nv_gpuinfo["gpu"]):
            print("  GPU %d: \t %s (%s GB, %s used)" % (
                i,
                ginfos["product_name"],
                ginfos["fb_memory_usage"]["total"],
                ginfos["fb_memory_usage"]["used"]
            ))
"""
