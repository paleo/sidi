import sys
import logging
import logging.config
import traceback
from io import TextIOWrapper
from threading import Thread

class StreamToLogger(TextIOWrapper):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    Reference:
    https://www.electricmonk.nl/log/2011/08/14/redirect-stdout-and-stderr-to-a-logger-in-python/
    """
    def __init__(self, logger, formatter):
        self.logger = logger
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.error(line.rstrip())

    def flush(self):
        return


class LogThread(Thread):
    """
    Reference: http://benno.id.au/blog/2012/10/06/python-thread-exceptions

    Exceptions captured with sys.excepthook are captured only at the process-scope,
    meaning that exceptions in Threads are *not* captured.
    Therefore, this wrapper is used around threading.Thread.

    LogThread should always be used in preference to threading.Thread.

    The interface provided by LogThread is identical to that of threading.Thread,
    however, if an exception occurs in the thread the error will be logged
    (using logging.exception) rather than printed to stderr.

    This is important in daemon style applications where stderr is redirected
    to /dev/null.
    """
    def __init__(self, logger, **kwargs):
        super().__init__(**kwargs)
        self._real_run = self.run
        self.run = self._wrap_run
        self.logger = logger

    def _wrap_run(self):
        try:
            self._real_run()
        except:
            self.logger.exception('Thread Exception:')



class Logger(object):
    def __init__(
        self,
        loggername,
        level="DEBUG",
        logfile="logger.log",
        console=True,
        capture_stderr=True,
    ):
        """
        Simple logging interface.

        Parameters
        ----------
        loggername: str
            Logger name.
        level: str
            Logging level. Can be "DEBUG", "INFO", "ERROR", "FATAL".
        logfile: str
            File where the log are written. If set to None, the logs are not written in a file.
        console: bool
            If set to True, the logs are (also) written in stdout/stderr
        capture_stderr: bool
            If set to True, stderr messages will also be written in logfile
        """

        self.loggername = loggername
        self.level = level
        self.logfile = logfile
        self.console = console
        self.capture_stderr = capture_stderr
        self.configure_logger()


    def configure_logger(self):
        conf = self.get_default_config_dict()
        for handler in conf["handlers"].keys():
            conf["handlers"][handler]["level"] = self.level.upper()
        conf["loggers"][self.loggername]["level"] = self.level.upper()
        if not(self.console):
            conf["loggers"][self.loggername]["handlers"].remove("console")

        self.config = conf
        logging.config.dictConfig(conf)
        self.logger = logging.getLogger(self.loggername)

        if self.capture_stderr:
            self.err_formatter = logging.Formatter(
                "%(message)s"
            )
            sys.stderr = StreamToLogger(self.logger, self.err_formatter)


    def info(self, msg):
        return self.logger.info(msg)

    def debug(self, msg):
        return self.logger.debug(msg)

    def warning(self, msg):
        return self.logger.warning(msg)

    def error(self, msg):
        return self.logger.error(msg)

    def fatal(self, msg):
        return self.logger.fatal(msg)

    def critical(self, msg):
        return self.logger.critical(msg)

    def __del__(self):
        if self.capture_stderr:
            sys.stderr = sys.__stderr__

    def get_default_config_dict(self):
        # TODO read from file
        conf = {
            'version': 1,
            'formatters': {
                'default': {'format': '%(asctime)s - %(levelname)s - %(message)s', 'datefmt': '%d-%m-%Y %H:%M:%S'},
                'console': {'formar': '%(message)s'}
            },
            'handlers': {
                'console': {
                    'level': 'DEBUG',
                    'class': 'logging.StreamHandler',
                    'formatter': 'console',
                    'stream': 'ext://sys.stdout'
                },
                'file': {
                    'level': 'DEBUG',
                    #~ 'class': 'logging.handlers.RotatingFileHandler',
                    'class': 'logging.FileHandler',
                    'formatter': 'default',
                    'filename': self.logfile,
                    'mode': 'w', # <= what is the "best" default ?
                    #~ 'maxBytes': 1048576,
                    #~ 'backupCount': 3
                }
            },
            'loggers': {
                self.loggername: {
                    'level': 'DEBUG',
                    'handlers': ['console', 'file'],
                    'propagate': True
                }
            },
            'disable_existing_loggers': False,
        }
        return conf


class DummyLogger(object):
    """
    Logger that is either a "true" logger object, or a fall-back to "print".

    In many cases, a Logger object is (optionally) passed as a parameter
    when creating an object instance, ex. m = MyClass(args, logger=logger).
    The target class code should adapt whether logger is None or not.
    To avoid many "if self.logger is not None", we use this DummyLogger class
    to fall-back on stdout when logger is None.
    """
    def __init__(self, logger):
        self.logger = logger
        methods = [
            "debug",
            "warn",
            "warning",
            "info",
            "error",
            "fatal",
            "critical",
            "exception"
        ]
        for method in methods:
            self.__setattr__(
                method,
                self.logger.__getattribute__(method) if self.logger is not None else print
            )

LogLevel = {
    "notset": logging.NOTSET,
    "debug": logging.DEBUG,
    "info": logging.INFO,
    "warn": logging.WARN,
    "warning": logging.WARNING,
    "error": logging.ERROR,
    "critical": logging.CRITICAL,
    "fatal": logging.FATAL
}



